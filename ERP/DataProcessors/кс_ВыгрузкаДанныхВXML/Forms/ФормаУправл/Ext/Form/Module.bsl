﻿#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ЗаписатьXMLВФайл(Команда)
	ЗаписатьXMLВФайлНаСервере();
КонецПроцедуры

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ЗаписатьXMLВФайлНаСервере()
	
	ПараметрыВыгрузки = Новый Структура;
	ПараметрыВыгрузки.Вставить("Период", Объект.Период);
	ПараметрыВыгрузки.Вставить("ИспользованиеСпискаТаблиц", Ложь);
	
	СтрокаXML = кс_ОбработкаДанныхДляОтчетаСравненияБазДанных.ПолучитьXMLДанные(ПараметрыВыгрузки);
	
	//ПутьКФайлуЗаписи = Объект.ВариантПодключения.ПутьКФайлу;
	ТекстовыйДокИзФайла = Новый ТекстовыйДокумент;
	ТекстовыйДокИзФайла.ДобавитьСтроку(СтрокаXML);
	ТекстовыйДокИзФайла.Записать(Объект.ПутьКФайлуЗаписи, КодировкаТекста.UTF8);
	
КонецПроцедуры


#КонецОбласти
